# Weather Icon View

#### 项目介绍
- 项目名称：Weather Icon View-ohos
- 所属系列：openharmony的第三方组件适配移植
- 功能：Weather Icon View-ohos提供显示天气图标的自定义视图
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta 1
- 基线版本：Release 1.1.0

#### 效果演示

![screen1](https://images.gitee.com/uploads/images/2021/0622/095649_a81798b0_9260176.gif "屏幕截图.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```

allprojects {

    repositories {

        maven {

            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'

        }

    }

}

 ```

2.在entry模块的build.gradle文件中，

 ```
 dependencies {

    implementation('com.gitee.chinasoft_ohos:WeatherIconView:0.0.1-SNAPSHOT')

    ......  

 }
 ```
#### 使用说明

1.将仓库导入到本地仓库中

2.在布局文件中加入WeatherIconView控件,代码实例如下:

```

<com.github.pwittchen.weathericonview.WeatherIconView
        ohos:id="$+id:weatherIconView"
        ohos:height="match_content"
        ohos:width="match_content"
        custom:weatherIconResource="$string:wi_day_sunny"
        custom:weatherIconSize="100"
        custom:weatherIconColor="$ohos:color:id_color_warning"
        />

```
3.代码中使用

```
WeatherIconView weatherIconView = (WeatherIconView)findComponentById(ResourceTable.Id_weatherIconView);
weatherIconView.setIconResource(getString(ResourceTable.String_wi_day_sunny));
        weatherIconView.setIconColor(Color.BLACK);
        weatherIconView.setIconSize(200);

```
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

* 0.0.1-SNAPSHOT

#### 版权和许可信息

```
    Copyright 2015 Piotr Wittchen

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

```