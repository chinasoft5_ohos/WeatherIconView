package com.github.pwittchen.weathericonview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.*;

public class WeatherIconView extends Text {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.DEBUG, 0, "TAG");

    public WeatherIconView(Context context) {
        super(context);
        initialize(context);
    }

    public WeatherIconView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initializeAttributes(context, attrSet);
        initialize(context);
    }

    public WeatherIconView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        initializeAttributes(context, attrs);
        initialize(context);
    }

    private void initialize(Context context) {
        if (onTextEditorAction()) {
            return;
        }
        ResourceManager resourceManager = getContext().getResourceManager();
        RawFileEntry rawFileEntry = resourceManager.getRawFileEntry("resources/rawfile/weather.ttf");
        Resource resource = null;
        try {
            resource = rawFileEntry.openRawFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuffer fileName = new StringBuffer("weather.ttf");
        File file = new File(getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName.toString());
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            int index;
            byte[] bytes = new byte[1024];
            while (resource != null && (index = resource.read(bytes)) != -1) {
                outputStream.write(bytes, 0, index);
                outputStream.flush();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resource != null) {
                    resource.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            Font.Builder builder = new Font.Builder(file);
            Font font = builder.build();
            setFont(font);
        }
    }

    private boolean onTextEditorAction() {
        return false;
    }

    private void initializeAttributes(Context context, AttrSet attrs) {
        if (onTextEditorAction()) {
            return;
        }
        initIconResource(context, attrs);
        initIconSize(context, attrs);
        initIconColor(context, attrs);
    }

    public void initIconResource(Context context, AttrSet attrSet) {
        if (attrSet.getAttr("weatherIconResource").isPresent()) {
            setText(attrSet.getAttr("weatherIconResource").get().getStringValue());
        } else {
            HiLog.error(LABEL, "attr weatherIconResource is not present");
            setText("");
        }
    }

    public void initIconSize(Context context, AttrSet attrSet) {
        if (attrSet.getAttr("weatherIconSize").isPresent()) {
            setTextSize(attrSet.getAttr("weatherIconSize").get().getIntegerValue(), Text.TextSizeType.FP);
        } else {
            HiLog.error(LABEL, "attr weatherIconSize is not present");
        }
    }

    public void initIconColor(Context context, AttrSet attrSet) {

        if (attrSet.getAttr("weatherIconColor").isPresent()) {
            setTextColor(attrSet.getAttr("weatherIconColor").get().getColorValue());
        } else {
            HiLog.error(LABEL, "attr weatherIconColor is not present");
            setTextColor(Color.BLACK);
        }
    }

    /**
     * sets weather icon basing on String resources
     * Icons are created from weather-icons TTF font by Erik Flowers
     * Full icons reference can be found at: http://erikflowers.github.io/weather-icons/
     *
     * @param iconCode icon code located in res/values/strings.xml file
     */
    public void setIconResource(String iconCode) {
        setText(iconCode);
    }

    /**
     * sets icon size
     *
     * @param size icon size as an integer; default size is equal to 100
     */
    public void setIconSize(int size) {
        setTextSize(size);
    }

    /**
     * sets icon color resource
     *
     * @param colorResource color resource - eg. Color.RED or any custom color; default is Color.BLACK
     */
    public void setIconColor(Color colorResource) {
        setTextColor(colorResource);
    }
}