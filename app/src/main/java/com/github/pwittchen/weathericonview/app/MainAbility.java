/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.github.pwittchen.weathericonview.app;

import com.github.pwittchen.weathericonview.WeatherIconView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbility extends Ability {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.DEBUG,0,"TAG");
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().setStatusBarColor(Color.getIntColor("#757575"));
        getWindow().setStatusBarVisibility(Component.VISIBLE);
        setUIContent(ResourceTable.Layout_ability_main);
        WeatherIconView weatherIconView = (WeatherIconView)findComponentById(ResourceTable.Id_weatherIconView);
        HiLog.error(LABEL,"weatherIconView--");


//        weatherIconView.setIconResource(getString(ResourceTable.String_wi_day_cloudy));
//        weatherIconView.setIconColor(Color.BLACK);
//        weatherIconView.setIconSize(200);
    }
}
